/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.model;

/**
 *
 * @author sala302b
 */
public class Contato {

    private String nome;
    private String telefone;
    private String Email;
    private String tipodecontato;

    public Contato() {
    }

    public Contato(String nome, String telefone, String Email, String tipodecontato) {
        this.nome = nome;
        this.telefone = telefone;
        this.Email = Email;
        this.tipodecontato = tipodecontato;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String Email) {
        this.Email = Email;
    }

    public String getTipodecontato() {
        return tipodecontato;
    }

    public void setTipodecontato(String tipodecontato) {
        this.tipodecontato = tipodecontato;
    }
    
    

}
